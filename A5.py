from flask import Flask, request, render_template

app = Flask(__name__)

fruitlist = {'apples','bananas','cherries'}

def configure_routes(app):

    @app.route('/', methods=['post', 'get'])
    def HomePage():
        message = ''
        
        if request.method == 'POST':
            fruitschecked = set(request.form.getlist('fruits'))
            fruitsunchecked = fruitlist - fruitschecked
            showstatname = request.form.get('showstatname')
        
            submits = {
                "Show checked items": str(list(fruitschecked)),
                "Show unchecked items":str(list(fruitsunchecked)),
                "Show status": str(showstatname in fruitschecked),
                None: ""}
                    
            message = "".join([submits[request.form.get(i)] for i in ["checked", "unchecked", "showstat"]])

        return render_template('tmp1.html', message=message)
    #...

configure_routes(app)

if __name__ == "__main__":
    app.run(debug=True)