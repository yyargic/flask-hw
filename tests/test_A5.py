from flask import Flask, render_template
import A5
from A5 import configure_routes

def test_something():
    app = Flask(__name__)
    configure_routes(app)

    client = app.test_client()
    url = '/'
    response = client.post(url)
    
    assert b"fruits" in response.data
